NAME = adbd

PROTOC ?= protoc
PKG_CONFIG ?= pkg-config

libasyncio_srcs := \
    debian/vendored-libraries/libasyncio/AsyncIO.cpp

libcrypto_utils_srcs := \
    debian/vendored-libraries/libcrypto_utils/android_pubkey.cpp

libdiagnose_usb_srcs := \
    debian/vendored-libraries/diagnose_usb/diagnose_usb.cpp

libadbd_auth_srcs := \
    debian/vendored-libraries/adbd_auth/adbd_auth.cpp

# Protobuf files are generated in debian/rules
libadb_protos_srcs := \
    proto/adb_known_hosts.pb.cc \
    proto/key_type.pb.cc \
    proto/pairing.pb.cc

libadb_crypto_srcs := \
    crypto/key.cpp \
    crypto/rsa_2048_key.cpp \
    crypto/x509_generator.cpp

libadb_tls_connection_srcs += \
    tls/adb_ca_list.cpp \
    tls/tls_connection.cpp

libadb_pairing_auth_srcs := \
    pairing_auth/aes_128_gcm.cpp \
    pairing_auth/pairing_auth.cpp

libadb_pairing_connection_srcs += \
    pairing_connection/pairing_connection.cpp

libadb_srcs := \
    adb.cpp \
    adb_io.cpp \
    adb_listeners.cpp \
    adb_mdns.cpp \
    adb_trace.cpp \
    adb_unique_fd.cpp \
    adb_utils.cpp \
    fdevent/fdevent.cpp \
    services.cpp \
    sockets.cpp \
    socket_spec.cpp \
    sysdeps/env.cpp \
    sysdeps/errno.cpp \
    transport.cpp \
    transport_fd.cpp \
    types.cpp

libadb_posix_srcs := \
    sysdeps_unix.cpp \
    sysdeps/posix/network.cpp

libadb_linux_srcs := \
    fdevent/fdevent_epoll.cpp

libadbd_core_srcs := \
    daemon/adb_wifi.cpp \
    daemon/auth.cpp \
    daemon/logging.cpp \
    daemon/transport_local.cpp \
    \
    daemon/usb.cpp \
    daemon/usb_ffs.cpp
# Excluded: daemon/jdwp_service.cpp
#           daemon/property_monitor.cpp
#           daemon/watchdog.cpp

libadbd_services_srcs := \
    daemon/file_sync_service.cpp \
    daemon/services.cpp \
    daemon/shell_service.cpp \
    shell_service_protocol.cpp \
    \
    daemon/mdns.cpp
# Excluded: daemon/abb_service.cpp
#           daemon/framebuffer_service.cpp
#           daemon/restart_service.cpp

adbd_srcs := \
    daemon/main.cpp

SOURCES := \
    $(libasyncio_srcs) \
    $(libcrypto_utils_srcs) \
    $(libdiagnose_usb_srcs) \
    $(libadbd_auth_srcs) \
    $(libadb_protos_srcs) \
    $(libadb_crypto_srcs) \
    $(libadb_tls_connection_srcs) \
    $(libadb_pairing_auth_srcs) \
    $(libadb_pairing_connection_srcs) \
    $(libadb_srcs) \
    $(libadb_posix_srcs) \
    $(libadb_linux_srcs) \
    $(libadbd_core_srcs) \
    $(libadbd_services_srcs) \
    $(adbd_srcs)

SOURCES_CPP = $(filter %.cpp,$(SOURCES))
OBJECTS_CPP = $(SOURCES_CPP:.cpp=.o)
SOURCES_CC = $(filter %.cc,$(SOURCES))
OBJECTS_CC = $(SOURCES_CC:.cc=.o)

CXXFLAGS += -fpermissive
CPPFLAGS += \
    -Idebian/stub-headers \
    -I. \
    -Idebian/vendored-libraries/libasyncio/include \
    -Idebian/vendored-libraries/libcrypto_utils/include \
    -Idebian/vendored-libraries/diagnose_usb/include \
    -Idebian/vendored-libraries/adbd_auth/include \
    -Iproto \
    -Icrypto/include \
    -Itls/include \
    -Ipairing_auth/include \
    -Ipairing_connection/include \
    -Ilibs/libadbd_fs/include \
    -I/usr/include/android -I/usr/include/android/include \
    -DADB_VERSION='"$(DEB_VERSION)"' -DADB_HOST=0 -D_GNU_SOURCE -DALLOW_ADBD_NO_AUTH=1 -DALLOW_ADBD_ROOT=1 \
    -DANDROID_BASE_UNIQUE_FD_DISABLE_IMPLICIT_CONVERSION=1 \
    $(shell pkg-config --cflags \
        libsystemd \
        libbrotlidec \
        libbrotlienc \
        liblz4 \
        libzstd \
        protobuf)

LDFLAGS += \
    -Wl,-rpath=/usr/lib/$(DEB_HOST_MULTIARCH)/android -Wl,-rpath-link=. \
    -lpthread -L/usr/lib/$(DEB_HOST_MULTIARCH)/android/ \
    -lc \
    -lbase \
    -lcutils \
    -llog \
    -landroid-properties \
    -ldns_sd \
    -lutil \
    -lcrypto \
    -lssl \
    -lresolv \
    $(shell $(PKG_CONFIG) --libs \
        libsystemd \
        libbrotlidec \
        libbrotlienc \
        liblz4 \
        libzstd \
        protobuf)

debian/out/$(NAME): $(OBJECTS_CC) $(OBJECTS_CPP) $(STATIC_LIBS)
	$(CXX) -o $@ $^ $(LDFLAGS)

$(OBJECTS_CPP): %.o: %.cpp
	$(CXX) -c -o $@ $< $(CXXFLAGS) $(CPPFLAGS)

$(OBJECTS_CC): %.o: %.cc
	$(CXX) -c -o $@ $< $(CXXFLAGS) $(CPPFLAGS)

