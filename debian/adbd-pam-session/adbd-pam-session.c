/*
 * Copyright (C) 2022 Guido Berhoerster <guido+ubports@berhoerster.name>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *  * Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *  * Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#include <err.h>
#include <errno.h>
#include <fcntl.h>
#include <grp.h>
#include <limits.h>
#include <paths.h>
#include <pwd.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ioctl.h>
#include <sys/wait.h>
#include <termios.h>
#include <time.h>
#include <unistd.h>
#include <utmpx.h>

#include <security/pam_appl.h>

#ifndef	HAVE_CLOSEFROM
#if (__GLIBC__ > 2) || (__GLIBC__ == 2 && __GLIBC__ >= 34)
#define	HAVE_CLOSEFROM
#endif /* (__GLIBC__ > 2) || (__GLIBC__ == 2 && __GLIBC__ >= 34) */
#endif /* !HAVE_CLOSEFROM */

#ifndef	UBUNTU_PHABLET
#define	UBUNTU_PHABLET	32011
#endif /* !UBUNTU_PHABLET */

#ifndef	SERVICE_NAME
#define	SERVICE_NAME	"adbd-pam-session"
#endif /* !SERVICE_NAME */

#ifndef	SERVICE_NAME_NONINTERACTIVE
#define	SERVICE_NAME_NONINTERACTIVE "adbd-pam-session-noninteractive"
#endif /* !SERVICE_NAME_NONINTERACTIVE */

#ifndef	KILL_TIMEOUT
#define	KILL_TIMEOUT 3 /* s */
#endif /* KILL_TIMEOUT */

#define	DEV_PREFIX	"/dev/"

volatile sig_atomic_t	fatal_signo;

#ifndef	HAVE_CLOSEFROM
void
closefrom(int from)
{
	long	fd, max;

	max = sysconf(_SC_OPEN_MAX);
	max = (max < 0) ? INT_MAX : max;
	for (fd = from; fd < max; fd++) {
		close(fd);
	}
}
#endif /* !HAVE_CLOSEFROM */

static void
utmp_write_entry(const char *tty, const char *name, int add)
{
	struct utmpx	utmpx = { 0 };
	struct timeval	tv;

	if (strncmp(tty, DEV_PREFIX, sizeof (DEV_PREFIX)-1) == 0) {
		tty += sizeof (DEV_PREFIX)-1;
	}

	utmpx.ut_type = add ? USER_PROCESS : DEAD_PROCESS;
	utmpx.ut_pid = getpid();
	strncpy(utmpx.ut_line, tty, sizeof (utmpx.ut_line));
	strncpy(utmpx.ut_user, name, sizeof (utmpx.ut_user));
	gettimeofday(&tv, NULL);
	utmpx.ut_tv.tv_sec = tv.tv_sec;
	utmpx.ut_tv.tv_usec = tv.tv_usec;

	setutxent();
	if (pututxline(&utmpx) == NULL) {
		warn("pututxline");
	}
	endutxent();
}

static int
env_setenv(char ***envp, const char *name, const char *value, int overwrite)
{
	char	**env = *envp;
	char	*var = NULL;
	size_t	name_len, n;

	name_len = strlen(name);
	for (n = 0; (env != NULL) && (env[n] != NULL); n++) {
		if ((strncmp(env[n], name, name_len) == 0) &&
		    (env[n][name_len] == '=')) {
			break;
		}
	}

	if (asprintf(&var, "%s=%s", name, value) < 0) {
		return (-1);
	}

	if ((env != NULL) && (env[n] != NULL)) {
		if (!overwrite) {
			return (0);
		}

		free(env[n]);
		env[n] = var;
		return (0);
	}

	if ((env = reallocarray(env, n + 1 + 1, sizeof (char *))) == NULL) {
		free(var);
		return (-1);
	}
	*envp = env;
	env[n++] = var;
	env[n] = NULL;

	return (0);
}

static int
env_update(char ***envp, const char *username, const char *home,
    const char *term)
{
	int	retval = 0;
	size_t	n;
	char	*path;

	if (env_setenv(envp, "HOME", home, 0) < 0) {
		return (-1);
	}

	if (env_setenv(envp, "USER", username, 1) < 0) {
		return (-1);
	}

	if (env_setenv(envp, "LOGNAME", username, 1) < 0) {
		return (-1);
	}

	if (env_setenv(envp, "TERM", term, 1) < 0) {
		return (-1);
	}

	n = confstr(_CS_PATH, NULL, 0);
	if (n > 0) {
		if ((path = malloc(n)) == NULL) {
			return (-1);
		}
		confstr(_CS_PATH, path, n);
		retval = env_setenv(envp, "PATH", path, 0);
		free(path);
	}

	return (retval);
}

static void
handle_signal(int signo)
{
	fatal_signo = signo;
}

int
main(int argc, char *argv[])
{
	int		status = 1;
	const char	*cmd = (argc == 2) ? argv[1] : NULL;
	uid_t		ruid, euid, suid;
	struct sigaction act = { 0 };
	struct sigaction ohupact, ointact, oquitact, otermact;
	struct passwd	*pw;
	char		*username = NULL;
	char		*shell = NULL;
	char		*home = NULL;
	const void	*item;
	gid_t		gid;
	char		*tty_tmp, *tty = NULL;
	int		pam_ret = PAM_SUCCESS;
	struct pam_conv	conv = { 0 };
	pam_handle_t	*pamh = NULL;
	char		**env;
	pid_t		pid;
	gid_t		rgid, egid, sgid;
	const char	*term;
	const char	*shell_name;
	char		shell_arg0[16];
	int		dev_null;
	pid_t		wpid;
	sigset_t	mask, omask;
	siginfo_t	winfo;
	int		signo = 0;

	if (getresuid(&ruid, &euid, &suid) != 0) {
		warn("getresuid");
		goto out;
	}

	/*
	 * This helper may be run as UBUNTU_PHABLET or root and may be setuid
	 * root or not. In case of the latter PAM session setup may fail
	 * depending on the PAM configuration
	 */
	if (((ruid != UBUNTU_PHABLET) && (ruid != 0)) ||
	    ((euid != UBUNTU_PHABLET) && (euid != 0))) {
		warnx("invalid user");
		goto out;
	}

	if (argc > 2) {
		warnx("invalid number of arguments");
		goto out;
	}

	closefrom(STDERR_FILENO + 1);

	/*
	 * keep track of fatal signal(s) in a variable, but do not terminate;
	 * this is necessary so that the PAM session can be cleaned up and the
	 * child can immediately exit if there was a fatal signal between here
	 * and the reset of signal handling after the fork
	 */
	act.sa_handler = handle_signal;
	sigfillset(&act.sa_mask);
	if ((sigaction(SIGHUP, &act, &ohupact) < 0) ||
	    (sigaction(SIGINT, &act, &ointact) < 0) ||
	    (sigaction(SIGQUIT, &act, &oquitact) < 0) ||
	    (sigaction(SIGTERM, &act, &otermact) < 0)) {
		warn("sigaction");
		goto out;
	}

	pw = getpwuid(UBUNTU_PHABLET);
	if ((pw == NULL) || (pw->pw_name == NULL)) {
		warnx("user not found");
		goto out;
	}
	username = strdup(pw->pw_name);
	if (username == NULL) {
		warn("strdup");
		goto out;
	}
	shell = strdup((pw->pw_shell != NULL) ? pw->pw_shell : _PATH_BSHELL);
	if (shell == NULL) {
		warn("strdup");
		goto out;
	}
	home = strdup((pw->pw_dir != NULL) ? pw->pw_dir : "/");
	if (home == NULL) {
		warn("strdup");
		goto out;
	}
	gid = pw->pw_gid;

	pam_ret = pam_start((cmd == NULL) ? SERVICE_NAME :
	    SERVICE_NAME_NONINTERACTIVE, username, &conv, &pamh);
	if (pam_ret != PAM_SUCCESS) {
		warnx("pam_start: %s", pam_strerror(pamh, pam_ret));
		goto out;
	}

	tty_tmp = ttyname(STDIN_FILENO);
	if (tty_tmp != NULL) {
		tty = strdup(tty_tmp);
	}

	if (tty != NULL) {
		pam_ret = pam_set_item(pamh, PAM_TTY, tty);
		if (pam_ret != PAM_SUCCESS) {
			warnx("pam_set_item: %s", pam_strerror(pamh, pam_ret));
			goto out;
		}
	}

	/*
	 * set to local user/localhost since authentication is the
	 * responsibility of adbd and there is no trusted remote user/host here
	 */
	pam_ret = pam_set_item(pamh, PAM_RUSER, username);
	if (pam_ret != PAM_SUCCESS) {
		warnx("pam_set_item: %s", pam_strerror(pamh, pam_ret));
		goto out;
	}

	pam_ret = pam_set_item(pamh, PAM_RHOST, "localhost");
	if (pam_ret != PAM_SUCCESS) {
		warnx("pam_set_item: %s", pam_strerror(pamh, pam_ret));
		goto out;
	}

	/* check whether user account is valid */
	pam_ret = pam_acct_mgmt(pamh, 0);
	if (pam_ret != PAM_SUCCESS) {
		warnx("pam_acct_mgmt: %s", pam_strerror(pamh, pam_ret));
		goto out;
	}

	/* verify the PAM username has not been changed */
	pam_ret = pam_get_item(pamh, PAM_USER, &item);
	if (pam_ret != PAM_SUCCESS) {
		warnx("pam_get_item: %s", pam_strerror(pamh, pam_ret));
		goto out;
	}
	if (strcmp(username, (const char *)item) != 0) {
		warnx("PAM username does not match: %s", (const char *)item);
		goto out;
	}

	/* initialize PAM credentials */
	pam_ret = pam_setcred(pamh, PAM_ESTABLISH_CRED);
	if (pam_ret != PAM_SUCCESS) {
		warnx("pam_setcred: %s", pam_strerror(pamh, pam_ret));
		goto out;
	}

	/* open session */
	pam_ret = pam_open_session(pamh, PAM_SILENT);
	if (pam_ret != PAM_SUCCESS) {
		warnx("pam_open_session: %s", pam_strerror(pamh, pam_ret));
		goto out;
	}

	/* add utmp entry */
	if (tty != NULL) {
		utmp_write_entry(tty, username, 1);
	}

	/* block SIGCHLD so it is not missed by the parent later */
	sigemptyset(&mask);
	sigaddset(&mask, SIGCHLD);
	sigprocmask(SIG_BLOCK, &mask, &omask);

	switch (pid = fork()) {
	case -1:
		warn("fork");
		goto delete_session;
	case 0: /* child */
		/* restore default signal handling */
		sigprocmask(SIG_UNBLOCK, &omask, NULL);
		if ((sigaction(SIGHUP, &ohupact, NULL) < 0) ||
		    (sigaction(SIGINT, &ointact, NULL) < 0) ||
		    (sigaction(SIGQUIT, &oquitact, NULL) < 0) ||
		    (sigaction(SIGTERM, &otermact, NULL) < 0)) {
			err(1, "sigaction");
		}
		if (fatal_signo > 0) {
			/* previous fatal signal in child or parent */
			_exit(128 + fatal_signo);
		}

		/* get PAM environment and end session */
		env = pam_getenvlist(pamh);
		pam_end(pamh, PAM_SUCCESS | PAM_DATA_SILENT);
		if (env == NULL) {
			errx(1, "pam_getenvlist failed");
		}

		/* transition uid/gid */
		if (euid == 0) {
			if (setresgid(gid, gid, gid) != 0) {
				err(1, "setresgid");
			}
			if (getresgid(&rgid, &egid, &sgid) != 0) {
				err(1, "getresgid");
			}
			if ((rgid != gid) || (egid != gid) || (sgid != gid)) {
				errx(1, "failed to change gid");
			}
			if (initgroups(username, gid) != 0) {
				err(1, "initgroups");
			}
			if (setresuid(UBUNTU_PHABLET, UBUNTU_PHABLET,
			    UBUNTU_PHABLET) != 0) {
				err(1, "setresuid");
			}
			if (getresuid(&ruid, &euid, &suid) != 0) {
				err(1, "getresuid");
			}
			if ((ruid != UBUNTU_PHABLET) ||
			    (euid != UBUNTU_PHABLET) ||
			    (suid != UBUNTU_PHABLET)) {
				errx(1, "failed to change uid");
			}
		}

		if (chdir(home) != 0) {
			warn("chdir");
			if (chdir("/") != 0) {
				warn("chdir");
			}
		}

		/* set up user environment variables */
		term = getenv("TERM");
		if (env_update(&env, username, home,
		    (term != NULL) ? term : "dumb") != 0) {
			err(1, "failed to set user enviroment variables");
		}

		/* by convention argv[0][0] == '-' indicates a login shell */
		shell_name = strrchr(shell, '/');
		snprintf(shell_arg0, sizeof (shell_arg0), "%s%s",
		    (cmd != NULL) ? "" : "-",
		    (shell_name != NULL) ?  shell_name + 1 : "");

		if (cmd != NULL) {
			execle(shell, shell_arg0, "-c", cmd, (char *)NULL, env);
		} else {
			execle(shell, shell_arg0, (char *)NULL, env);
		}
		err(1, "execle");
	default: /* parent */
		closefrom(STDERR_FILENO + 1);
		dev_null = open("/dev/null", O_RDWR);
		if (dev_null < 0) {
			warn("open");
			goto kill_child;
		}

		if ((dup2(dev_null, STDIN_FILENO) < 0) ||
		    (dup2(dev_null, STDOUT_FILENO) < 0) ||
		    (dup2(dev_null, STDERR_FILENO) < 0)) {
			warn("dup2");
			goto kill_child;
		}
		close(dev_null);
	}

	/*
	 * wait for either SIGCHLD or one of the fatal signals if no fatal
	 * signal was received previously
	 */
	sigaddset(&mask, SIGHUP);
	sigaddset(&mask, SIGINT);
	sigaddset(&mask, SIGQUIT);
	sigaddset(&mask, SIGTERM);
	sigprocmask(SIG_BLOCK, &mask, NULL);
	if (fatal_signo == 0) {
		while (((signo = sigwaitinfo(&mask, &winfo)) < 0) &&
		    (errno == EINTR));
		if (signo == SIGCHLD) {
			goto reap_child;
		}
		fatal_signo = signo;
	}

kill_child:
	kill(-pid, SIGTERM); /* kill child pgroup */

	/* wait with timeout for child to exit */
	sigemptyset(&mask);
	sigaddset(&mask, SIGCHLD);
	while (((signo = sigtimedwait(&mask, &winfo,
	    &(const struct timespec){ .tv_sec = KILL_TIMEOUT })) < 0) &&
	    (errno == EINTR));
	if (signo == SIGCHLD) {
		goto reap_child;
	}

	kill(-pid, SIGKILL); /* kill child pgroup */

reap_child:
	while (((wpid = waitid(P_PID, pid, &winfo, WEXITED)) < 0) &&
	    (errno != EINTR));

delete_session:
	/* remove utmp entry */
	if (tty != NULL) {
		utmp_write_entry(tty, username, 0);
	}

	pam_setcred(pamh, PAM_DELETE_CRED);
	pam_ret = pam_close_session(pamh, 0);

	if (fatal_signo > 0) {
		status = 128 + fatal_signo;
	} else if (pam_ret == PAM_SUCCESS) {
		status = 0;
	}

out:
	free(tty);
	free(home);
	free(shell);
	free(username);
	if (pamh != NULL) {
		pam_end(pamh, pam_ret);
	}

	exit(status);
}
